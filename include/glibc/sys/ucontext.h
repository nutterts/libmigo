// libmigo - include ucontext.h for target

#define __NO_UCONTEXT_H__

#ifdef __arm__
	#include <sys/arm/ucontext.h>
	#undef __NO_UCONTEXT_H__
#endif

#ifdef __NO_UCONTEXT_H__
	#error Missing sys/ucontext.h for compiler target
#endif
