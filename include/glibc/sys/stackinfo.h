// libmigo - include stackinfo.h for target

#define __NO_STACKINFO_H__

#ifdef __arm__
	#include <sys/arm/stackinfo.h>
	#undef __NO_STACKINFO_H__
#endif

#ifdef __NO_STACKINFO_H__
	#error Missing sys/stackinfo.h for compiler target
#endif
