// libmigo - include pthreadtypes.h for target

#define __NO_PTHREADTYPES_H__

#ifdef __arm__
	#include <bits/arm/pthreadtypes.h>
	#undef __NO_PTHREADTYPES_H__
#endif

#ifdef __NO_PTHREADTYPES_H__
	#error Missing bits/pthreadtypes.h for target
#endif
