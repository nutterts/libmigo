// libmigo - include endian.h for target

#define __NO_ENDIAN_H__

#ifdef __arm__
	#include <bits/arm/endian.h>
	#undef __NO_ENDIAN_H__
#endif

#ifdef __NO_ENDIAN_H__
	#error Missing bits/endian.h for target
#endif
