// libmigo - include setjmp.h for target

#define __NO_SETJMP_H__

#ifdef __arm__
	#include <bits/arm/setjmp.h>
	#undef __NO_SETJMP_H__
#endif

#ifdef __NO_SETJMP_H__
	#error Missing bits/setjmp.h for target
#endif
