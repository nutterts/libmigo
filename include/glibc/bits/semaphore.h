// libmigo - include semaphore.h for target

#define __NO_SEMAPHORE_H__

#ifdef __arm__
	#include <bits/arm/semaphore.h>
	#undef __NO_SEMAPHORE_H__
#endif

#ifdef __NO_SEMAPHORE_H__
	#error Missing bits/semaphore.h for target
#endif
